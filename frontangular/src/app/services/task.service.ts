import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Task } from './../interfaces/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private api = 'http://127.0.0.1';

  constructor(
    private http: HttpClient
  ) { }


  getTask(fullname: string) {
    const path = `${this.api}/person/${fullname}`;
    return this.http.get<Task>(path);
  }

  createTask(task: Task) {
    const path = `${this.api}/person`;
    return this.http.post(path, task);
  }


}
