import { Component } from '@angular/core';
import { TaskService } from './services/task.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-http-client';

  constructor(
    private taskService: TaskService
  ) {}

  

  getTask() {
    this.taskService.getTask('2')
    .subscribe(task => {
      console.log(task);
    });
  }

  createTask() {
    const task = {
      fullname: 'Test_Angular',
      date: '2020-08-23'
    };
    this.taskService.createTask(task)
    .subscribe((newTask) => {
      console.log(newTask);
    });
  }

  


}