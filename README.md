# TEST REST API (Guía de instalación para ambiente de desarrollo)

# 1. Descripción del proyecto

El proyecto test_rest_api esta construido en Java web application y contiene los métodos POST, GET de la tabla Personas.


Requisitos para el proyecto

Instalar Postman como cliente para consumir los metodos (Deseable)
Instalar Postgresql para base de datos.
Acceso al repositorio del proyecto RestApi (https://bitbucket.org/vasquezed/restapi/src/master/).


# 2. Instalación

## 2.1 Prerequisitos

* La siguiente instalación es para el sistema operativo ubuntu.


## 1.2 Instalar el software necesario
* Abrir una terminal y escribir:
    * ```sudo apt-get update && sudo apt-get -y upgrade```
    * ```sudo apt-get install git postgresql postgresql-contrib python-dev python3-dev python3-pip libpq-dev curl```
    * ```sudo snap install postman```

### 1.2.1 Instalar paquetes python requeridos
* ```pip3 install virtualenv```
* ```pip3 install virtualenvwrapper```

## 1.3 Corregir problema con las flechas para uso del editor vi
* Editar el archivo .vimrc agregando la siguiente línea asi:
    * ```vi ~/.vimrc```
    * Linea: ```set nocompatible```
    * Salir del editor con la secuencia de teclas ```esc :wq```

## 1.4 Instalación de Docker Compose
* Abrir una terminal y escribir:
	* ```sudo apt-get install docker-compose```

## 1.5 Instalación de Docker Compose
* Abrir una terminal y escribir:
	* ```sudo apt-get install docker-compose```


## 1.6 Instalación de postgres con Docker Compose
* Crear las carpetas requeridas
	* ```sudo mkdir /home/docker/```
	* ```sudo mkdir /home/docker/postgres/```

* Crear archivo .yml con los parametros requeridos
	* ```sudo vi docker-compose.yml```

	```
	version: '2'
	services:
	  postgres:
	    image: 'postgres:latest'
	    restart: always
	    volumes:
	      - './postgres_data:/var/lib/postgresql/data'
	    environment:
	      - POSTGRES_PASSWORD=admin
	    ports:
	      - '5432:5432'

	```

* Subir los servicios de docker
	* ```sudo docker-compose up -d```

* Validar que el servicio se encuentre activo
	* ```sudo docker ps```


## 1.7 Creación de base de datos y tablas

* Conectarse a la base de datos a través de docker
	* ```sudo docker exec -it postgres_postgres_1 psql -U postgres```

	* ```CREATE DATABASE test_rest_api;```

	* ```\c test_rest_api;```

	* ```create table Person ( fullname varchar(255) not null primary key, birth date not null);```


## 1.8 Crear la carpeta que va a contener los ambientes virtuales
* ```mkdir ~/codigo/.virtualenvs```

## 1.9 Editar .bashrc y agregar las lineas siguientes así:
* ```vi ~/.bashrc``` 
* ```export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3```
* ```export WORKON_HOME=~/codigo/.virtualenvs```
* ```source ~/.local/bin/virtualenvwrapper.sh```

## 1.10 Activar cambios del .bashrc
* ```source ~/.bashrc```

## 1.11 Crear virtualenv del proyecto para que utilice python 3.5
* ```mkvirtualenv -a ~/codigo/Prueba_BdB/test_rest_api_repository --python=/usr/bin/python3.5 venv_test_rest_api```

## 1.13 Instalar requerimientos del proyecto (requirements.txt)
* ```workon venv_test_rest_api```
* ```pip3 install -r requirements.txt```

## 1.14 Iniciar servidor de desarrollo para la aplicación
* En la consola digitar
    * ```python manage.py runserver```

## 1.15 Pruebas Unitarias
* Las pruebas unitarias se realizaron con pytest.
    * ```pytest rest_api_app/tests.py```